"""
Created: 2025-01

@author: SNM
"""

import argparse, pathlib
from Bio import SeqIO
from Bio.Seq import Seq
import pandas as pd

def append_new_sample_names_to_existing_csv(previous_label_list_file,column_number,input_biopython_file,output_csv_file,biopython_file_type):




	sample_ids_in_new_msa = []

	previous_label_list_df = pd.read_csv(previous_label_list_file)

	column_name = previous_label_list_df.columns.tolist()[column_number-1]


	for sample in SeqIO.parse(input_biopython_file, biopython_file_type):
		sample_ids_in_new_msa.append(sample.name)

	#with open('out_file.csv','w') as fo:
	#	for element in out_names:
	#		fo.write(element+',\n')
	#

	new_samples = [item for item in sample_ids_in_new_msa if item not in previous_label_list_df[column_name].values]



	new_samples_df = pd.DataFrame(new_samples, columns=[column_name])
	result_df = pd.concat([previous_label_list_df, new_samples_df], ignore_index=True)

	#check for duplicates
	duplicates = result_df[result_df.duplicated(keep=False)]  # keep=False shows all duplicates
	if not duplicates.empty:
		print()
		print('Attention! Duplicate samples may be present in the new output file!')
		print()

	result_df.to_csv(output_csv_file, index=False)
#append_new_sample_names_to_existing_csv()


if __name__ == "__main__":
	parser = argparse.ArgumentParser(description="Append record.names from a biopython SeqIO readable filetype to an existing csv. Potential duplicates are not considered.  Remove all positions of a phylip multiple sequence alignment, that does not match a certain threshold percentage of allowed chars ('GACT-', not allowed: '?N').\nThe output file will be named '<INPUT-FILE>.pthreshold.phy'.\n\tv.01\n\tcontact: s.meckoni@tu-bs.de")
	parser.add_argument('-i','--input', type=pathlib.Path, metavar='INPUT', help='Path to input csv file', required=True)
	parser.add_argument('-f','--fileof', type=pathlib.Path, metavar='INPUT', help='Path to biopython readable file from which the new sample names are read from', required=True)
	parser.add_argument('-o','--output', type=pathlib.Path, help='Path to output csv, standard would be input csv with _with_new_samples.csv appended.')
	parser.add_argument('--type', type=str, help='e.g.: phylip, phylip-relaxed or fasta.',required=True)
	parser.add_argument('--column', type=int, help='no. of column of the csv to which the new sample names should be added, default = 1.',default=1)
	args = parser.parse_args()

	if args.output is None:
		args.output = str(args.input) + '_with_new_samples.csv'
		print('Output file will be named:',args.output)
	append_new_sample_names_to_existing_csv(args.input, args.column, args.fileof, args.output, args.type)



