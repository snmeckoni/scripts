#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
This script iterates the result csv from DeepLoc-2.0 to filter it for Extracellular localization. The resulted gene IDs and their corresponding Extracellular probability are printed out. Its  Made by SNM in 2023.
'''

from Bio import SeqIO


deeploc2_reuslts_csv = "/example/path/to/DeepLoc-2.0-result.csv"

localization = "Extracellular"
threshold = 0.8


def iterate_deeploc2_results_csv(deeploc2_reuslts_csv, localization, threshold):
    with open(deeploc2_reuslts_csv, 'r') as f:
        f.readline()
        line = f.readline()
        while line:
            if line.split(',')[1] == localization or float(','.join(line.split(',')[5:6])) > threshold: #in this line 5:6 is specifying the Extracellular column of the result csv.
                print()
                print(line.split(',')[0].split('||')[4] + ' ' + ','.join(line.split(',')[5:6]))
            line = f.readline()

iterate_deeploc2_results_csv(deeploc2_reuslts_csv, localization, threshold)
