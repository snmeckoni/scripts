#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import json, argparse, pathlib
from argparse import RawTextHelpFormatter
import pandas as pd

parser = argparse.ArgumentParser(description='\tDescription:\n\tGet peptide fasta from a JSON file (which itself can come from GenBank through this tool: https://chlorobox.mpimp-golm.mpg.de/GenBank-JSON-Converter.html). The fasta file will be generated and named as specified. But the parent directory of this file needs to be created manually.\n\tv.1.1\n\tcontact: s.meckoni@tu-bs.de',formatter_class=RawTextHelpFormatter)
parser.add_argument('in_JSON', type=pathlib.Path, metavar='INPUT', help='Path to input JSON file')
parser.add_argument('out_fasta', type=pathlib.Path, metavar='OUTPUT', help='Path to output fasta file')
args = parser.parse_args()

def json_pandas_parser( args ):
    with open(args.in_JSON, "r") as json_data:
      data = json.loads(json_data.read())
      df = pd.DataFrame.from_dict(pd.json_normalize(data,"features"), orient='columns')
    with open(args.out_fasta, 'w') as out:
        for index, row in df.iterrows():
            if ( row['type'] == 'cds' or row['type'] == 'CDS' ) and str(row['translation']) != 'nan':
                out.write('>'+str(row['gene'])+' '+str(row['product'])+'\n'+row['translation']+'\n')

json_pandas_parser( args )
