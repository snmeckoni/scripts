# by SNM 2024

from Bio import SeqIO

import argparse
import pathlib
import sys


def parse_arguments(args):
    # Create an ArgumentParser object
    parser = argparse.ArgumentParser(description='No. of N in fasta/fastq')

    parser.add_argument(
            '-t', '--type', type=pathlib.Path,
            help='f or q to specify the file format of the input file. If it no information is given, the program tries to interfer the file format by itself.',
            )

    parser.add_argument(
            '-i', '--input', type=pathlib.Path,
            help='FASTA or FASTQ input file',
            #required=True,
            )

    parser.add_argument(
            '--version', action='store_true',
            help='output version information and exit.',
            )

    # Parse the command-line arguments
    args = parser.parse_args(args)
    return args


def iterate_input_file(args):

    fasta_suffixes_list = ['.fa','.FASTA','.fn','.fan','.fasta']
    fastq_suffixes_list = ['.q','.ASTQ','.fastq']

    if args.type == 'f':
        fast_file_type = 'fasta'
    elif args.type == 'q':
        fast_file_type = 'fastq'
    else:
        print("No file type specified, trying to interfer file type from file name ending")
        if args.input.suffix in fasta_suffixes_list:
            fast_file_type = 'fasta'
            print('fasta spotted')
        if args.input.suffix in fastq_suffixes_list:
            fast_file_type = 'fastq'
            print('fastq spotted')

    total_bases = 0

    n_bases = 0

    for record in SeqIO.parse(args.input, fast_file_type):
        total_bases += len(record.seq)
        n_bases += record.seq.count('N')

    print(args.input, ' contains:')
    print('Total bases: ', total_bases)
    print('from that:')
    print('Total Ns:    ', n_bases)
    print('Percentage:  ', n_bases / total_bases * 100)
#
#    print("%s %i" % (record.id, len(record)))




if __name__ == "__main__":


    sysargs_to_parse = sys.argv[1:]

    if not sysargs_to_parse:
        sysargs_to_parse.append('-h')
    
    arguments = parse_arguments(sysargs_to_parse)

    if arguments.version:
        print('version 1')
        raise SystemExit(0)
    iterate_input_file(arguments)
