# small scripts
This repository contains small scripts that I made.
## append_new_sample_names_to_csv_from_biopython_seqio_v1.py
```bash
usage: append_new_sample_names_to_csv_from_biopython_seqio_v1.py [-h] -i INPUT -f INPUT [-o OUTPUT] --type TYPE [--column COLUMN]

Append record.names from a biopython SeqIO readable filetype to an existing csv. Potential duplicates are not considered. Remove all positions of a phylip multiple sequence alignment, that does not match a
certain threshold percentage of allowed chars ('GACT-', not allowed: '?N'). The output file will be named '<INPUT-FILE>.pthreshold.phy'. v.01 contact: s.meckoni@tu-bs.de

options:
  -h, --help            show this help message and exit
  -i INPUT, --input INPUT
                        Path to input csv file
  -f INPUT, --fileof INPUT
                        Path to biopython readable file from which the new sample names are read from
  -o OUTPUT, --output OUTPUT
                        Path to output csv, standard would be input csv with _with_new_samples.csv appended.
  --type TYPE           e.g.: phylip, phylip-relaxed or fasta.
  --column COLUMN       no. of column of the csv to which the new sample names should be added, default = 1.
```
## calc_N_bases_in_fast_v1.py
Calculate the number of total bases and how many of them are "N" and the ratio of "N"s to the total amount, in a given FASTA or FASTQ file.
## DEG_Kallisto_to_DESeq2_v1.R
This script was used in my thesis for calculating DEGs (differential expressed genes) from kallisto and a SRA-runs metadata list.
## DESeq2_results_trimmer_v2.py
This script was used in my thesis to generate FASTA files containing the sequences of GOIs.
## DeepLoc-2.0_results_trimmer.py
This script iterates the result csv from [DeepLoc-2.0](https://services.healthtech.dtu.dk/services/DeepLoc-2.0/) to filter it for Extracellular localization. The resulted gene IDs and their corresponding Extracellular probability are printed out.
## FASTQ_extractor_from_FAST5_mapping_file.py
This script takes a fastq file (gzipped or not) and extracts reads based on a mapping file from ont_fast5_api (available on [github](https://github.com/nanoporetech/ont_fast5_api)). The output fastq files will be saved in a specified directory. According to the mapping file the output fastq reads can also be divided into multiple files.
## FASTQ_stats3_graphical_v0.2.py
1. This script creates a histogramm of the reads within a fastq file not fasta file!
2. Give a '--in_file' (only fastq but may be compressed e.g.: "xyz.fastq.gz") and a full path to the output file (this will be a picture, .png extension recommended, if other extension are possible was not tested).
3. Optional: Give a bin size. This will be defining the width of the bars (i.e. how reads will be represented by a bar). A good bin size is depending on the data. Standard is 10000 but for data with the a longest read of 50000 bp a bin size smaller than 5000 is recommended. Also the bin size can be adjusted by the wished result or once own preferences. The standard value will be 10000. Users are highly encouraged to try different binsizes to find a good one :)

* Please note: This is a "quick and dirty" script that got no further revision except some comments and the argument parsing capabilities
Usage:
```
python3 FASTQ_stats3_graphical_v0.2.py

Mandatory:
--in_file <FULL_PATH_TO_FASTQ_FILE>
--output_file <FULL_PATH_TO_OUTPUT_PNG_FILE>

Optional:
--bin_size <INTEGER>
```
## JSON2fasta_v1.1.py
```
usage: JSON2fasta_v1.1.py [-h] INPUT OUTPUT

Description:
Get peptide fasta from a JSON file (which itself can come from GenBank through this tool: https://chlorobox.mpimp-golm.mpg.de/GenBank-JSON-Converter.html. The fasta file will be generated and named as specified. But the parent directory of this file needs to be created manually.

contact: the authors

positional arguments:
  INPUT       Path to input JSON file
  OUTPUT      Path to output fasta file

options:
  -h, --help  show this help message and exit
```
## Statistics_and_Boxplots_v1.py
This script was used in my thesis for calculating some statistics and generating graphics.
## shasta_wrapper_v01.sh
This script runs shasta. The main advantage is, that only the input folder needs to be given and all files inside of this input folder are passed to shasta. Untested!
```bash
Usage: ./shasta_wrapper_v01.sh -o assemblyDirectory -i input_data_folder [-t no_of_threads] [-s shasta_bin_path] [-c config_path]
```
## shasta_busco_wrapper_v01.sh
Like the shasta wrapper, but with an automatically busco run following the assembly. Untested!
```bash
Usage: ./shasta_busco_wrapper_v01.sh -o assemblyDirectory -i input_data_folder -O BUSCO_out_path [-t no_of_threads] [-s shasta_bin_path] [-c config_path] [-b busco_bin_path] [-l busco_lineage]
```
## small_heatmap_v1.py
This script was used in my thesis for generating a heatmap with per row normalised tpms from kallisto and a goi list.
## Reference
For FASTQ_extractor_from_FAST5_mapping_file.py and JSON2fasta_v1.1.py: Samuel Nestor Meckoni, Benneth Nass, and Boas Pucker (2023) ‘Phylogenetic placement of <em>Ceratophyllum submersum</em> based on a complete plastome sequence derived from nanopore long read sequencing data’, bioRxiv, p. 2023.06.27.546741. Available at: https://doi.org/10.1101/2023.06.27.546741.
