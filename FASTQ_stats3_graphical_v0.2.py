### S. N. Meckoni ###
### s.meckoni@tu-bs.de ###
### a modified script from: ###

### Boas Pucker ###
### bpucker@cebitec.uni-bielefeld.de ###
### v0.25 ###


__usage__ = """
        python3 FASTQ_stats3_graphical_v0.2.py
        
        Mandatory:
        --in_file <FULL_PATH_TO_FASTQ_FILE>
        --output_file <FULL_PATH_TO_OUTPUT_PNG_FILE>
        
        Optional:
        --bin_size <INTEGER>
        
        
        original script: bug reports and feature requests: bpucker@cebitec.uni-bielefeld.de
        
		modified script: contact: s.meckoni@tu-bs.de



 0.: This script creates a histogramm of the reads within a fastq file not
     fasta file!

 1.: Give a '--in_file' (only fastq but may be compressed e.g.: "xyz.fastq.gz")
     and a full path to the output file (this will be a picture, .png extension
     recommended, if other extension are possible was not tested).

 2.: Optional: Give a bin size. This will be defining the width of the bars (i.e. how
     reads will be represented by a bar). A good bin size is depending on the
     data. Standard is 10000 but for data with the a longest read of 50000 bp a
     bin size smaller than 5000 is recommended. Also the bin size can be
     adjusted by the wished result or once own preferences. The standard value
     will be 10000. Users are highly encouraged to try different binsizes to
     find a good one :)

-1.: This is a "quick and dirty" script that got no further revision except
     this comments and the argument parsing capabilities
     

'''

Explanation about big changes to the original script:
    Since the calculate_n50 function does exit as soon as the n50 value was
    calculated, i made a second n50 function. The list created for all reads
    in the original calculate_n50 function are used for the graph generation.
    This is, why the commands to generate the graph are within the original
    calculate_n50 function. And since the n50 value is incorporated into the
    graph as well, another n50 calculation function was needed.

'''

"""

import sys, gzip, glob
import timeit
#import pandas as pd
import matplotlib.pyplot as plt
#from array import array

# --- end of imports --- #


def calculate_n50_2( total_length ):
    
    total = sum( total_length )
    half = total / 2.0
    sorted_lengths = sorted( total_length )[::-1]   
    counter = 0
    i = 0
    for i in range( len( sorted_lengths ) ):
        counter += sorted_lengths[ i ]
        if counter >= half:
            return sorted_lengths[ i ]
    sys.exit( "ERROR: no read lengths detected." )
    
    

def calculate_n50( total_length, output_file, bin_size ):
    """! @brief calculate N50 based on list of given read lengths """
        
    sorted_lengths2 = sorted(total_length)
    
    #create bins and count the amount of total nucleotides represented by all the reads in each bin:
    x_list = [0]    #inculding the bins
    y_list = []     #list of the basepair/nucleotide count per bin
    i_alt = bin_size
    y = 0
    for i in sorted_lengths2:
        if i >= i_alt:
            while i >= i_alt:
                i_alt += bin_size
                x_list.append(x_list[-1]+bin_size)
                y_list.append(y)
                y=0
        y += i
    y_list.append(y)
    #print('last ten reads: ',sorted_lengths2[-10:])
    #print(y_list, len(y_list))
    #print(x_list, len(x_list))
    
    
    #x = array("i", x_list)
    #y = array("i", y_list)
    
    cm = 1/2.54
    
    fig, ax = plt.subplots(figsize=(20*cm, 10*cm))
    
    bar_labels = 'N50'
    
    ax.bar(x=x_list,height=y_list, width=(bin_size*0.9),align='edge')
    #print(y)
    '''Axis.set_major_formatter(formatter)[
    !?!?!?!?!?!?!?!?!?!?!?!?!?!?!?!?!?!?!?!?!
    '''
    n50_wert = calculate_n50_2( total_length )
    
    count=0
    for i in x_list:
        count += 1
        if n50_wert >= i:
            #n50_x=i+bin_size
            n50_x=i
            ycount=count-1
    n50_y=y_list[ycount]
    
    ax.bar(x=n50_x,height=n50_y, width=(bin_size*0.9),label=bar_labels,align='edge')
    ax.set_ylabel('cumulated read length in bp')
    ax.set_xlabel('read length in bp')
    ax.set_title('summed-up read histogram')
    #ax.legend(title='Fruit color')   
    ax.legend()
    #plt.show()
    plt.savefig(output_file, dpi= 300)  
    return n50_wert


def analyze_FASTQ( filename, output_file, bin_size ):
    """! @brief analysis of FASTQ file """
    
    gzip_state = False
    try:
        file_extension = filename.split('.')[-1]
        if file_extension in [ "gz", "gzip", "GZ", "GZIP" ]:
            gzip_state = True
    except:
        pass
    
    if not gzip_state:
        with open( filename, "r" ) as f:
            total_length = []
            total_GC = []
            line = f.readline()    #header
            while line:
                seq = str( f.readline().strip().upper() )
                total_length.append( len( seq ) )
                total_GC.append( seq.count('C') )
                total_GC.append( seq.count('G') )
                f.readline()    #useless line
                f.readline()    #quality line
                line = f.readline()
            print()
            print( "number of reads: " + str( len( total_length ) ) )
            total_len = sum( total_length )
            total_gc = sum( total_GC )
            n50 = calculate_n50( total_length, output_file, bin_size )
            #print( filename )
            print( "total number of nucleotides:\t" + str( total_len ) )
            print( "average read length:\t" + str( total_len / float( len( total_length ) ) ) )
            print( "GC content:\t" + str( total_gc / float( total_len ) ) )
            print( "N50: " + str( n50 ) )
        
    else:
        with gzip.open( filename, "rb" ) as f:
            total_length = []
            total_GC = []
            line = f.readline()    #header
            while line:
                seq = str( f.readline().strip().upper() )
                total_length.append( len( seq ) )
                total_GC.append( seq.count('C') )
                total_GC.append( seq.count('G') )
                f.readline()    #useless line
                f.readline()    #quality line
                line = f.readline()
            print()
            print( "number of reads: " + str( len( total_length ) ) )
            total_len = sum( total_length )
            total_gc = sum( total_GC )
            n50 = calculate_n50( total_length, output_file, bin_size )
            #print( filename )
            print( "total number of nucleotides:\t" + str( total_len ) )
            print( "average read length:\t" + str( total_len / float( len( total_length ) ) ) )
            print( "GC content:\t" + str( total_gc / float( total_len ) ) )
            print( "N50: " + str( n50 ) )


def main( arguments ):
    """! @brief runs everything """
    input_file = arguments[ arguments.index( '--in_file' )+1 ]
    output_file = arguments[ arguments.index( '--output_file' )+1 ]
    if '--bin_size' in arguments:
        bin_size = int(arguments[ arguments.index( '--bin_size' )+1 ])
    else:
        bin_size = 10000
    
    start = timeit.timeit()
    
    analyze_FASTQ( input_file, output_file, bin_size )
    
    end = timeit.timeit() # das funktioniert leider nicht richtig xD
    print()
    print ('Zeit zum ausführen', end - start)
    '''
    else:
        #directory = arguments[ arguments.index( '--in_dir' )+1 ]
        directory = ''
        if directory[-1] != '/':
            directory += "/"
        input_files = []
        extensions = [ ".fq", ".fastq", ".fq.gzip", ".fastq.gzip", ".fq.gz", ".fastq.gz", ".FQ", ".FASTQ", ".FQ.GZIP", ".FASTQ.GZIP", ".FQ.GZ", ".FASTQ.GZ" ]
        for extension in extensions:
            input_files += glob.glob( directory + '*' + extension )
        for filename in input_files:
            try:
                analyze_FASTQ( filename )
            except:
                print( "ERROR while processing " + filename )
'''

if __name__ == '__main__':
    #main(  )
    if '--in_file' in sys.argv or '--output_file' in sys.argv:
        main( sys.argv )
    else:
        sys.exit( __usage__ )
    
