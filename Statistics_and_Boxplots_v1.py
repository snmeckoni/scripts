#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SNM 2023-11-23

import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from scipy import stats


df1 = pd.read_csv("/example/path/to/input.csv")


print()
res = stats.shapiro(df1.loc[df1['deficiency'] == '+']['no. of traps'])
print('Is the data normal distributed (Shapiro-Wilk test)?')
print('+, no. of traps:', stats.shapiro(df1.loc[df1['deficiency'] == '+']['no. of traps']))
print('N-, no. of traps:', stats.shapiro(df1.loc[df1['deficiency'] == 'N-']['no. of traps']))
print('N-, no. of traps, with removed outlier (value=0):', stats.shapiro(df1.loc[df1['deficiency'] == 'N-'].loc[df1['no. of traps'] != 0]['no. of traps']))
print('+, no. of branches:', stats.shapiro(df1.loc[df1['deficiency'] == '+']['no. of branches']))
print('N-, no. of branches:', stats.shapiro(df1.loc[df1['deficiency'] == 'N-']['no. of branches']))
print()
print('two-sample t-test')
print('no. of traps:', stats.ttest_ind(df1.loc[df1['deficiency'] == 'N-']['no. of traps'],df1.loc[df1['deficiency'] == '+']['no. of traps']))
print('no. of branches:', stats.ttest_ind(df1.loc[df1['deficiency'] == 'N-']['no. of branches'],df1.loc[df1['deficiency'] == '+']['no. of branches']))
print('two-sample t-test with removed outlier')
print('no. of traps:', stats.ttest_ind(df1.loc[df1['deficiency'] == 'N-'].loc[df1['no. of traps'] != 0]['no. of traps'],df1.loc[df1['deficiency'] == '+']['no. of traps']))
print()
print("Welch's t-test")
print('no. of traps:', stats.ttest_ind(df1.loc[df1['deficiency'] == 'N-']['no. of traps'],df1.loc[df1['deficiency'] == '+']['no. of traps'],equal_var=False))
print('no. of branches:', stats.ttest_ind(df1.loc[df1['deficiency'] == 'N-']['no. of branches'],df1.loc[df1['deficiency'] == '+']['no. of branches'],equal_var=False))
print("Welch's t-test with removed outlier")
print('no. of traps:', stats.ttest_ind(df1.loc[df1['deficiency'] == 'N-'].loc[df1['no. of traps'] != 0]['no. of traps'],df1.loc[df1['deficiency'] == '+']['no. of traps'],equal_var=False))
print()
print("Levene's test with outlier")
print('no. of traps:', stats.levene(df1.loc[df1['deficiency'] == 'N-']['no. of traps'],df1.loc[df1['deficiency'] == '+']['no. of traps']))
print('no. of branches:', stats.levene(df1.loc[df1['deficiency'] == 'N-']['no. of branches'],df1.loc[df1['deficiency'] == '+']['no. of branches']))
print("Levene's test with removed outlier")
print('no. of traps:', stats.levene(df1.loc[df1['deficiency'] == 'N-'].loc[df1['no. of traps'] != 0]['no. of traps'],df1.loc[df1['deficiency'] == '+']['no. of traps']))
print()



# Plots
# https://stackoverflow.com/questions/36578458/how-does-one-insert-statistical-annotations-stars-or-p-values/37518947#37518947
# https://matplotlib.org/stable/gallery/statistics/boxplot_demo.html#sphx-glr-gallery-statistics-boxplot-demo-py

f, axes = plt.subplots(1, 2)

#1. subplot
sns.boxplot(x="deficiency", y="no. of traps", data=df1, palette="PRGn_r", ax=axes[0])

# statistical annotation
x1, x2 = 0,1  # columns 'Sat' and 'Sun' (first column: 0, see plt.xticks())
y, h, col = df1['no. of traps'].max() + 2, 2, 'k'


axes[0].plot([x1, x1, x2, x2], [y, y+h, y+h, y], lw=1.5, c=col)
axes[0].text((x1+x2)*.5, y+h, "**", ha='center', va='bottom', color=col)
axes[0].set_title('traps', fontsize=14)

#2. subplot
sns.boxplot(x="deficiency", y="no. of branches", data=df1, palette="PiYG_r", ax=axes[1])

# statistical annotation
x1, x2 = 0,1
y, h, col = df1['no. of branches'].max() + 2, 2, 'k'

axes[1].plot([x1, x1, x2, x2], [y, y+h, y+h, y], lw=1.5, c=col)
axes[1].text((x1+x2)*.5, y+h, "*", ha='center', va='bottom', color=col)
axes[1].set_title('branches', fontsize=14)


plt.show()
