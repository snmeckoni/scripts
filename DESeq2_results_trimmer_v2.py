#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# by SNM 2023

from Bio import SeqIO



def deg_trimmer(in_table, threshold):
    goi_list = []
    with open(in_table, 'r') as f:
        line = f.readline()
        line = f.readline()
        #print(line)
        while line:
            if line.split(',')[2] != 'NA':
                if float(line.split(',')[2]) > 0 and float(line.split(',')[2]) <= threshold:
                    goi_list.append(line.split(',')[0])
            line = f.readline()
    return goi_list

def loc_trimmer(in_table, threshold, localisation, goi_list):
    best_hits_list = []
    with open(in_table, 'r') as f2:
        line = f2.readline()
        line = f2.readline()
        while line:
            if str('"'+line.split(',')[0][:-8]+'"') in goi_list and float(line.split(',')[localisation]) >= threshold:
                best_hits_list.append(line.split(',')[0])
            line = f2.readline()
    return best_hits_list

def extract_goi_from_fasta(in_fasta, goi_header_list, out_fasta):
    input_seq_iterator = SeqIO.parse(in_fasta, "fasta")
    short_seq_iterator = (record for record in input_seq_iterator if str(goi_header_list)[2:-2] in str(record.id))

    SeqIO.write(short_seq_iterator, out_fasta, "fasta-2line")


deg_in_table = '6.R_output/DEG_high_in_bladder.csv'
threshold = 0.05

in_fasta = "1.raw_data/Utricualria_gibba_genome_structural_annotation/29027-CDS-prot_removed_whitespace.fasta"
out_fasta = '2023-08-09_accurate_v2_confident_0_05_CDS-prot_bladder_extracellular.fasta'

extract_goi_from_fasta(in_fasta, deg_trimmer(deg_in_table, threshold), out_fasta)


print(deg_trimmer(deg_in_table, threshold))
print(len(deg_trimmer(deg_in_table, threshold)))

