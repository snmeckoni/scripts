#! /usr/bin/bash

# This script is mainly there to be able to specify an input data folder from which every file is taken as the input for shasta.
# 2024-12-17 v01 by SNM

# Default values
# It can make sense to adjust these settings in the script if you run shasta often with the same settings e.g. with the same config
shasta_bin_path=shasta #can be a path to a bin

config_path=/path/to/Nanopore-May2022_adj_min_read_length.conf

no_of_threads=4


# Function to display usage
usage() {
    echo "Usage: $0 -o assemblyDirectory -i input_data_folder [-t no_of_threads] [-s shasta_bin_path] [-c config_path]"
    exit 1
}


# Parse command-line arguments
while getopts ":o:i:t:s:c:" opt; do
  case $opt in
    o)
      outdirpath="$OPTARG"
      ;;
    i)
      input_data_folder="$OPTARG"
      ;;
    t)
      no_of_threads="$OPTARG"
      ;;
    s)
      shasta_bin_path="$OPTARG"
      ;;
    c)
      config_path="$OPTARG"
      ;;
    \?)
      usage
      ;;
  esac
done


# Check if mandatory arguments are set
if [[ -z "$outdirpath" || -z "$input_data_folder" ]]; then
  usage
fi



# get input file list from folder
files=$(find "$input_data_folder" -maxdepth 1 -type f | tr '\n' ' ')

sudo $shasta_bin_path --threads $no_of_threads --input $files --config $config_path --memoryBacking 2M --memoryMode filesystem --assemblyDirectory $outdirpath
