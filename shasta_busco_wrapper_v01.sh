#! /usr/bin/bash

# This script is mainly there to be able to specify an input data folder from which every file is taken as the input for shasta. And to automatically run BUSCO, once shasta finishes.
# 2024-12-18 v01 by SNM

# Default values
# It can make sense to adjust these settings in the script if you run shasta often with the same settings e.g. with the same config
#
shasta_bin_path=shasta #can be a path to a bin

busco_bin_path=busco
# an alternative that I use is the docker version of busco which can be given with additional arguments by putting everything in quotes:
# busco_bin_path="sudo docker run --rm -v /vol/data:/vol/data ezlabgva/busco:v5.7.1_cv1 busco"

config_path=/path/to/Nanopore-May2022_adj_min_read_length.conf

no_of_threads=4

busco_lineage=embryophyta_odb10


# Function to display usage
usage() {
    echo "Usage: $0 -o assemblyDirectory -i input_data_folder -O BUSCO_out_path [-t no_of_threads] [-s shasta_bin_path] [-c config_path] [-b busco_bin_path] [-l busco_lineage]"
    exit 1
}


# Parse command-line arguments
while getopts ":o:i:t:s:c:b:O:l:" opt; do
  case $opt in
    o)
      asm_outdirpath="$OPTARG"
      ;;
    i)
      input_data_folder="$OPTARG"
      ;;
    t)
      no_of_threads="$OPTARG"
      ;;
    s)
      shasta_bin_path="$OPTARG"
      ;;
    c)
      config_path="$OPTARG"
      ;;
    c)
      busco_bin_path="$OPTARG"
      ;;
    O)
      busco_outdirpath="$OPTARG"
      ;;
    l)
      busco_lineage="$OPTARG"
      ;;
    \?)
      usage
      ;;
  esac
done


# Check if mandatory arguments are set
if [[ -z "$asm_outdirpath" || -z "$input_data_folder" ]]; then
  usage
fi



# get input file list from folder
files=$(find "$input_data_folder" -maxdepth 1 -type f | tr '\n' ' ')

$shasta_bin_path --threads $no_of_threads --input $files --config $config_path --memoryBacking 2M --memoryMode filesystem --assemblyDirectory $asm_outdirpath \
	&& $busco_bin_path -i $asm_outdirpath/Assembly.fasta -m genome --cpu $no_of_threads -l $busco_lineage --out_path $busco_outdirpath
