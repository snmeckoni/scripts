#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on May 2023

@author: SNM
"""


import gzip, Bio, os, subprocess, csv
from Bio import SeqIO


def load_mapping_file_in_dict(in_mapping_file):
    a = {}
    with open(in_mapping_file) as f:
        for line in f:
            (k, v) = line.split()
            a[k] = v
    return a

def iterate_reads_from_fastq(infastq, outfastq_path, mapping_dict):
    file_list = []
    with open(infastq, 'r') as fastqin:
        for record in Bio.SeqIO.parse(fastqin, format='fastq'):
            if str(record.id) in mapping_dict:
                if mapping_dict[str(record.id)] not in file_list:
                    file_list.append(mapping_dict[str(record.id)])
                    with open(outfastq_path+'/'+mapping_dict[str(record.id)][:-1]+'q', 'w') as outfastq:
                        SeqIO.write(record, outfastq, "fastq")
                else:
                    with open(outfastq_path+'/'+mapping_dict[str(record.id)][:-1]+'q', 'a') as outfastq:
                        SeqIO.write(record, outfastq, "fastq")

def gzip_iterate_reads_from_fastq(infastq, outfastq_path, mapping_dict):
    file_list = []
    with gzip.open(infastq, 'rt') as fastqin:
        for record in Bio.SeqIO.parse(fastqin, format='fastq'):
            if str(record.id) in mapping_dict:
                if mapping_dict[str(record.id)] not in file_list:
                    file_list.append(mapping_dict[str(record.id)])
                    with gzip.open(outfastq_path+'/'+mapping_dict[str(record.id)][:-1]+'q.gz', 'wt') as outfastq:
                        SeqIO.write(record, outfastq, "fastq")
                else:
                    with gzip.open(outfastq_path+'/'+mapping_dict[str(record.id)][:-1]+'q.gz', 'at') as outfastq:
                        SeqIO.write(record, outfastq, "fastq")


infastq = ''

outfastq_path = ''

in_mapping_file = ''


gzip_iterate_reads_from_fastq(infastq,outfastq_path,load_mapping_file_in_dict(in_mapping_file))
