# -*- coding: utf-8 -*-
"""
2023
SNM
"""

import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pylab as plt


def construct_heatmap_from_pd_df(df):
    plt.style.use("seaborn")
    df_norm_row = df.apply(lambda x: (x-x.mean())/x.std(), axis = 1)
    heat_map = sns.heatmap( df_norm_row,  cmap="crest", annot=df, fmt=".1f")
    plt.show()

def get_tpms_from_kallisto_abundances_file(srr_ids, gene_ids, path_to_kallisto_results_folder, condition_names):
    first = True
    for element, name in zip(srr_ids, condition_names):
        df = pd.read_csv(path_to_kallisto_results_folder + "/" + element + "/abundance.tsv", sep="\t")
        if first == True:
            df4 = concat_pd_and_rename(gene_ids, df, name)
            first = False
        else:
            df5 = concat_pd_and_rename(gene_ids, df, name)
            df4 = pd.concat([df4,df5], axis=1)
    df4['GOIs'] = gene_ids
    df4 = df4.set_index('GOIs')
    tissue = ['vegetative','bladder']
    deficiency = ["N","P"]
    df4.columns = pd.MultiIndex.from_product([deficiency, tissue], names=['deficiency', 'tissue'])
    return df4

def get_gene_ids_from_goi_lst(path_to_goi_lst):
    gene_ids = []
    with open(path_to_goi_lst, 'r') as f:
        line = f.readline()
        gene_ids.append(line.strip())
        while line:
            line = f.readline()
            if line != "":
                gene_ids.append(line.strip())
    return gene_ids

def concat_pd_and_rename(gene_ids, df, condition_name):
    first = True
    for gene_id in gene_ids:
        if first == True:
            df1 = df[df['target_id'].str.contains(gene_id)][['target_id', 'tpm']]
            first = False
        else:
            df2 = df[df['target_id'].str.contains(gene_id)][['target_id', 'tpm']]
            df1 = pd.concat([df1,df2])
    df1 = df1.set_index(['target_id']).rename(columns={'tpm':condition_name})
    return df1


srr_ids = ['SRR8717098','SRR8717099'] # N deficiency
srr_ids = ['SRR8717098','SRR8717099','SRR8717100','SRR8717107'] # N and P deficiency
condition_names = ['veg. tissue','bladder','veg. tissue','bladder']
path_to_goi_lst = './goi_list.txt'
path_to_kallisto_results_folder = "./1.kallisto_output/"

construct_heatmap_from_pd_df(get_tpms_from_kallisto_abundances_file(srr_ids, get_gene_ids_from_goi_lst(path_to_goi_lst), path_to_kallisto_results_folder, condition_names))
